from pymongo import MongoClient

from config_data.config import settings

db_connect = MongoClient(settings.db.url)

collection_db = db_connect[settings.db.NAME_DB][settings.db.NAME_COLLECTION]
