import asyncio
import logging
import sys

from aiogram import Bot, Dispatcher
from config_data.config import settings

from handlers import data_router


async def main():
    bot: Bot = Bot(
        token=settings.bot.token
    )
    dp: Dispatcher = Dispatcher()
    dp.include_router(data_router)

    await bot.delete_webhook(drop_pending_updates=True)
    await dp.start_polling(bot)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, stream=sys.stdout)
    asyncio.run(main())
