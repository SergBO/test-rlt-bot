import json

from aiogram import Router
from aiogram.types import Message
from aiogram.filters import CommandStart

from crud.db import get_salary

router = Router()


@router.message(CommandStart())
async def cmd_star(message: Message):
    await message.answer(text="Привет я телеграмм бот для выполения тестового задания")


@router.message()
async def answer_message(message: Message):
    msg = json.loads(message.text)
    dt_from = msg['dt_from']
    dt_upto = msg['dt_upto']
    group_type = msg['group_type']
    print(msg)
    result = get_salary(dt_from, dt_upto, group_type)
    await message.answer(result)
