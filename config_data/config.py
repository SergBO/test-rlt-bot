import os
from dotenv import load_dotenv
from pydantic import BaseModel
from pydantic_settings import BaseSettings

load_dotenv()


class DbSettings(BaseModel):
    url: str = os.getenv("DB_URL")
    NAME_DB: str = "sampleDB"
    NAME_COLLECTION: str = "sample_collection"


class BotConfig(BaseModel):
    token: str = os.getenv("BOT_TOKEN")


class Settings(BaseSettings):
    db: DbSettings = DbSettings()
    bot: BotConfig = BotConfig()


settings = Settings()
