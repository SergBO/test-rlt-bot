## Настройка базы данных для локального тестирования
- Поднять базу данных
```commandline
docker compose up -d
```
- Заполнить базу данных
```commandline
mongorestore sampleDB/sample_collection.bson
```